<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller {
	/**
	* Use contructor to load library session and url helper in the whole class
	*/
	public function __construct() {
		parent::__construct();
		// Load the session library
		$this->load->library('session');
		$this->load->helper('url');
	}

	/**
	 * Index Page for this controller.
	 */
	public function index()
	{
		/*$this->session->set_flashdata('msg_info', 'value');
		$this->session->set_flashdata('msg_info_title', 'value title');
		$this->session->set_flashdata('msg_error', 'value');
		$this->session->set_flashdata('msg_error_title', 'value title');
		$this->session->set_flashdata('msg_warning', 'value');
		$this->session->set_flashdata('msg_warning_title', 'value title');
		$this->session->set_flashdata('msg_success', 'value');
		$this->session->set_flashdata('msg_success_title', 'value title');*/
		$this->load->view('EPFL_menu');
	}

	/**
	* Login Page
	*/
	public function login()
	{
		// Load the EPFL auth library
		$this->load->library('EPFL_auth') ;
		$this->EPFL_auth = new EPFL_auth();

		// Set the application name (displayed in the tequila login page)
		$this->EPFL_auth->SetApplicationName('CI EPFL');
		// Set the wanted attributes
		// 'name', 'firstname', 'displayname', 'username', 'personaltitle','statut', 'classe', 'email', 'title', 'title-en', 'unit','unit-en', 'where', 'wheres',  'office', 'phone',  'uniqueid',  'unixid',  'groupid',  'org', 'categorie', 'allunits', 'unitid', 'cf', 'allcfs', 'role-respadmin', 'role-respaccred', 'role-respinfo', 'role-respinfra', 'role-respcomm', 'role-respsecu', 'droit-accreditation', 'droit-smsweb', 'droit-gestionprofils', 'droit-cartevisite', 'droit-demdetrav', 'droit-admindiode', 'droit-sre', 'droit-vpnguest', 'droit-distrilog', 'droit-admingaspar', 'droit-adminroles', 'droit-smssmtp', 'droit-intranet', 'droit-railticket', 'droit-inventaire', 'droit-controlesf', 'droit-payonline', 'droit-adminad', 'droit-demvm', 'droit-msdnaa', 'droit-impression', 'droit-substitutiondistrilog', 'droit-confirmdistrilog', 'droit-shopepfl', 'droit-ficheporte', 'camiprocardid', 'unitresp', 'group', 'group2', 'memberof'
		$this->EPFL_auth->SetWantedAttributes(array('uniqueid','displayname','name','firstname','email','unit', 'unitid', 'where', 'group'));
		$this->EPFL_auth->SetWishedAttributes(array('email', 'title'));

		// Application and server
		$this->EPFL_auth->SetApplicationURL('http://ciepfl.dev/auth/login');
		$this->EPFL_auth->SetServer('https://tequila.epfl.ch');
		$this->EPFL_auth->SetServerURL('https://tequila.epfl.ch/cgi-bin/tequila');

		// Some parameters and filters
		#$this->EPFL_auth->SetCustomFilter('org=EPFL&firstname=John&unit=SC-PME&where=SC-PME/SC-S/ETU/EPFL/CH&group=inbc');
		#$this->EPFL_auth->SetCustomParamaters(array ('toto' => 1));
		// If identities = one, the user have to specify on unit when logged in
		$this->EPFL_auth->SetCustomParamaters(array ('identities' => 'one'));
		$this->EPFL_auth->Authenticate ();
		$this->session->set_flashdata('msg_success_title', 'Login successful');
		$this->session->set_flashdata('msg_success', 'Hey, ' . $this->EPFL_auth->getValue('displayname') . ' !');
		$userdata = array(
									'sciper' => $this->EPFL_auth->getValue('uniqueid'),
									'displayname' => $this->EPFL_auth->getValue('displayname'),
									'name' => $this->EPFL_auth->getValue('name'),
									'firstname' => $this->EPFL_auth->getValue('firstname'),
									'email' => $this->EPFL_auth->getValue('email'),
									'unit' => $this->EPFL_auth->getValue('unit'),
									'unitid' => $this->EPFL_auth->getValue('unitid'),
									'groups' => $this->EPFL_auth->getValue('group'),
		);
		$this->session->set_userdata('Tequila',$userdata);

		// @TODO create model EPFL_auth.php to save these values in DB

		redirect(base_url());
	}

	/**
	* Logout Page
	*/
	public function logout()
	{
		// Load the EPFL auth library
		$this->load->library('EPFL_auth') ;
		$this->EPFL_auth = new EPFL_auth();
		// Clear CI session
		$this->session->sess_destroy();
		// And create a new one for flash messages
		$this->session->sess_create();
		$this->session->set_flashdata('msg_info_title', 'Sucessfuly logged out');
		$this->session->set_flashdata('msg_info', 'Bye bye !');
		// The got to Tequila log out URL
		redirect($this->EPFL_auth->getLogoutUrl(base_url()));
	}
}

/* End of file auth.php */
/* Location: ./application/controllers/auth.php */
