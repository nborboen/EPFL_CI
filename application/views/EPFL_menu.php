<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Welcome to EPFL Auth</title>
  <style type="text/css">
    ::selection {
      background-color: #E13300;
      color: white;
    }
    ::moz-selection {
      background-color: #E13300;
      color: white;
    }
    ::webkit-selection {
      background-color: #E13300;
      color: white;
    }
    body {
      background-color: #fff;
      margin: 40px;
      font: 13px/20px normal Helvetica, Arial, sans-serif;
      color: #4F5155;
      margin: 0;
      padding: 0;
    }
    a {
      color: #003399;
      background-color: transparent;
      font-weight: normal;
    }
    h1 {
      color: #444;
      background-color: transparent;
      border-bottom: 1px solid #D0D0D0;
      font-size: 19px;
      font-weight: normal;
      margin: 0 0 14px 0;
      padding: 14px 15px 10px 15px;
    }
    code {
      font-family: Consolas, Monaco, Courier New, Courier, monospace;
      font-size: 12px;
      background-color: #f9f9f9;
      border: 1px solid #D0D0D0;
      color: #002166;
      display: block;
      margin: 14px 0 14px 0;
      padding: 12px 10px 12px 10px;
    }
    p.footer {
      text-align: right;
      font-size: 11px;
      border-top: 1px solid #D0D0D0;
      line-height: 32px;
      padding: 0 10px 0 10px;
      margin: 20px 0 0 0;
    }
    #main {
      margin: 0 15px 0 15px;
    }
    #container {
      margin: 60px;
      border: 1px solid #D0D0D0;
      -webkit-box-shadow: 0 0 8px #D0D0D0;
    }
    #EPFLmenu {
      height: 30px;
      margin: 10px;
      padding: 10px;
      text-align: center;
      border: 1px solid #D0D0D0;
      -webkit-box-shadow: 0 0 8px #D0D0D0;
    }
    ul#EPFLmenu li {
      line-height: 30px;
      display: inline;
      padding: 0 0.5em;
      float: right;
      /*border-left: 1px dashed black;*/
    }
    div ul#EPFLmenu li:not(:last-child) {
        border-left: 1px dotted black; /* limits the scope of the previous rule */
    }
    ul#EPFLmenu {
      list-style-type: none;
    }
    .message {
      -webkit-background-size: 40px 40px;
      -moz-background-size: 40px 40px;
      background-size: 40px 40px;
      background-image: -webkit-gradient(linear, left top, right bottom, color-stop(.25, rgba(255, 255, 255, .05)), color-stop(.25, transparent), color-stop(.5, transparent), color-stop(.5, rgba(255, 255, 255, .05)), color-stop(.75, rgba(255, 255, 255, .05)), color-stop(.75, transparent), to(transparent));
      background-image: -webkit-linear-gradient(135deg, rgba(255, 255, 255, .05) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .05) 50%, rgba(255, 255, 255, .05) 75%, transparent 75%, transparent);
      background-image: -moz-linear-gradient(135deg, rgba(255, 255, 255, .05) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .05) 50%, rgba(255, 255, 255, .05) 75%, transparent 75%, transparent);
      background-image: -ms-linear-gradient(135deg, rgba(255, 255, 255, .05) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .05) 50%, rgba(255, 255, 255, .05) 75%, transparent 75%, transparent);
      background-image: -o-linear-gradient(135deg, rgba(255, 255, 255, .05) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .05) 50%, rgba(255, 255, 255, .05) 75%, transparent 75%, transparent);
      background-image: linear-gradient(135deg, rgba(255, 255, 255, .05) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .05) 50%, rgba(255, 255, 255, .05) 75%, transparent 75%, transparent);
      -moz-box-shadow: inset 0 -1px 0 rgba(255, 255, 255, .4);
      -webkit-box-shadow: inset 0 -1px 0 rgba(255, 255, 255, .4);
      box-shadow: inset 0 -1px 0 rgba(255, 255, 255, .4);
      width: 100%;
      border: 1px solid;
      color: #fff;
      padding: 15px;
      position: fixed;
      _position: absolute;
      text-shadow: 0 1px 0 rgba(0, 0, 0, .5);
      -webkit-animation: animate-bg 5s linear infinite;
      -moz-animation: animate-bg 5s linear infinite;
    }
    .info {
      background-color: #4ea5cd;
      border-color: #3b8eb5;
    }
    .error {
      background-color: #de4343;
      border-color: #c43d3d;
    }
    .warning {
      background-color: #eaaf51;
      border-color: #d99a36;
    }
    .success {
      background-color: #61b832;
      border-color: #55a12c;
    }
    .message h3 {
      margin: 0 0 5px 0;
    }
    .message p {
      margin: 0;
    }
    @-webkit-keyframes animate-bg {
      from {
        background-position: 0 0;
      }
      to {
        background-position: -80px 0;
      }
    }
    @-moz-keyframes animate-bg {
      from {
        background-position: 0 0;
      }
      to {
        background-position: -80px 0;
      }
    }
  </style>
  <script src="http://code.jquery.com/jquery-latest.min.js"></script>
  <script>
    var myMessages = ['info', 'warning', 'error', 'success']; // define the messages types
    function hideAllMessages() {
      var messagesHeights = new Array(); // this array will store height for each
      for (i = 0; i < myMessages.length; i++) {
        messagesHeights[i] = $('.' + myMessages[i]).outerHeight();
        $('.' + myMessages[i]).css('top', -messagesHeights[i]); //move element outside viewport
      }
    }
    function showMessage(type) {
      $('.' + type + '-trigger').click(function() {
        hideAllMessages();
        $('.' + type).animate({
          top: "0"
        }, 500);
      });
    }
    $(document).ready(function() {
      // Initially, hide them all
      hideAllMessages();
      <?php echo ($this->session->flashdata('msg_info')) ? '$("#msg_info").animate({top: "0"}, 500);':''; ?>
      <?php echo ($this->session->flashdata('msg_warning')) ? '$("#msg_warning").animate({top: "0"}, 500);':''; ?>
      <?php echo ($this->session->flashdata('msg_error')) ? '$("#msg_error").animate({top: "0"}, 500);':''; ?>
      <?php echo ($this->session->flashdata('msg_success')) ? '$("#msg_success").animate({top: "0"}, 500);':''; ?>
      // When message is clicked, hide it
      $('.message').click(function() {
        $(this).animate({
          top: -$(this).outerHeight()
        }, 500);
      });
    });
  </script>
</head>

<body>

  <div class="messages">
    <div id="msg_info" class="info message">
      <h3><?php echo $this->session->flashdata('msg_info_title'); ?></h3>
      <p><?php echo $this->session->flashdata('msg_info'); ?></p>
    </div>
    <div id="msg_error" class="error message">
      <h3><?php echo $this->session->flashdata('msg_error_title'); ?></h3>
      <p><?php echo $this->session->flashdata('msg_error'); ?></p>
    </div>
    <div id="msg_warning"class="warning message">
      <h3><?php echo $this->session->flashdata('msg_warning_title'); ?></h3>
      <p><?php echo $this->session->flashdata('msg_warning'); ?></p>
    </div>
    <div id="msg_success" class="success message">
      <h3><?php echo $this->session->flashdata('msg_success_title'); ?></h3>
      <p><?php echo $this->session->flashdata('msg_success'); ?></p>
    </div>
  </div>

  <div id="container">
    <h1>Welcome to EPFL auth</h1>
    <div id="main">
      <ul id="EPFLmenu">
      <?php if (!$this->session->userdata('Tequila')) { ?>
        <li><a href="<?php echo site_url('auth/login'); ?>">Login</a></li>
      <?php } else { ?>
        <li><a href="<?php echo site_url('auth/logout'); ?>">Logout</a></li>
        <li>Welcome <a href="http://people.epfl.ch/<?php echo $this->session->userdata('Tequila')['sciper'] ; ?>"><?php echo $this->session->userdata('Tequila')['displayname'] ; ?></a></li>
      <?php } ?>
      </ul>

    <?php if ($this->session->userdata('Tequila')) { ?>
      <h3>Your Tequila data are:</h3>
      <code><pre><?php print_r($this->session->userdata('Tequila')); ?></pre></code>
      <hr />
    <?php } ?>

      <p>EPFL CI use Tequila (<a href="https://tequila.epfl.ch/intro.html">https://tequila.epfl.ch/intro.html</a>).
        This is a very basic website to show integration of Tequila into Code Igniter.</p>

      <p>If you would like to edit this page you'll find it located at:</p>
      <code>application/views/EPFL_menu.php</code>

      <p>The corresponding controller for this page is found at:</p>
      <code>application/controllers/auth.php</code>

    </div>

    <p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds</p>
  </div>

</body>
</html>
