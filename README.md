## EPFL CI
EPFL CI is a integration of [Tequila](https://tequila.epfl.ch/) into [CodeIgniter](https://ellislab.com/codeigniter).

## Info
@author	Nicolas Borboën  
@authoremail	<nicolas.borboen@epfl.ch>  
@authorlink	[Nicolas Borboën](http://people.epfl.ch/169419)  
@gitrepo	git clone https://git.epfl.ch/repo/ci.git  

## EPFL Template
An integration in EPFL "Web2010" template is done in the branche [epfl_template](https://gitlab.epfl.ch/nborboen/EPFL_CI/tree/epfl_template).

## Files
* This README file: `/var/www/epfl_ci/README.md`
* The Controller `/var/www/epfl_ci/application/controllers/auth.php`
* The Lib `/var/www/epfl_ci/application/libraries/EPFL_auth.php`
* The Model (@TODO) `/var/www/epfl_ci/application/models/EPFL_auth.php`
* The SQL (@TODO) `/var/www/epfl_ci/application/sql/EPFL_auth.sql`

# Tips
Use flash messages

    $this->load->library('session');

    $this->session->set_flashdata('msg_info', 'value');

    $this->session->set_flashdata('msg_info_title', 'value title');

    $this->session->set_flashdata('msg_error', 'value');

    $this->session->set_flashdata('msg_error_title', 'value title');

    $this->session->set_flashdata('msg_warning', 'value');

    $this->session->set_flashdata('msg_warning_title', 'value title');

    $this->session->set_flashdata('msg_success', 'value');

    $this->session->set_flashdata('msg_success_title', 'value title');

## Logs
* 2015-10-11       moved to glilab.epfl.ch
* 2014-08-13	   first useable version.
* 2014-08-08	   starting the project.